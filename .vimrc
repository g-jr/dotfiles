syntax on

set hidden
set tabstop=2
set shiftwidth=2
set expandtab
set softtabstop=2
set number
set hlsearch
set ruler
set ai
set is 
set autoindent
set ignorecase
set smartcase
set backspace=2
set splitbelow
set splitright
set shortmess-=S

nnoremap <Leader>b :ls<CR>:b<Space>

set laststatus=2
set statusline=
set statusline +=%1*\ %n\ %*            "buffer number
set statusline +=%5*%{&ff}%*            "file format
set statusline +=%3*%y%*                "file type
set statusline +=%4*\ %<%F%*            "full path
set statusline +=%2*%m%*                "modified flag
set statusline +=%1*%=%5l%*             "current line
set statusline +=%2*/%L%*               "total lines
set statusline +=%1*%4v\ %*             "virtual column number
set statusline +=%2*0x%04B\ %*          "character under cursor


map <Leader>/ :noh<CR>

set wildmenu
set wildmode=longest:full,full

"colorscheme peachpuff

function! MyHighlight() abort
  colorscheme peachpuff
  highlight visual cterm=bold ctermbg=Blue ctermfg=NONE
  highlight Comment cterm=bold ctermfg=green
  hi Search cterm=NONE ctermfg=Black ctermbg=Cyan

  hi StatusLine ctermfg=15 guifg=#ffffff ctermbg=239 guibg=#4e4e4e cterm=bold gui=bold

  set cursorline
  set cursorlineopt=number
  "highlight CursorLineNr cterm=NONE ctermbg=15 ctermfg=8 gui=NONE guibg=#ffffff guifg=#d70000
  highlight CursorLineNr cterm=NONE ctermbg=White ctermfg=Black gui=NONE
  hi LineNrr cterm=NONE guifg=#505050 guibg=Black
  hi Normal guifg=White guibg=Black

endfunction

augroup MyColors
  autocmd!
  autocmd colorscheme * call MyHighlight()
augroup END

"colorscheme peachpuff


