export PS1="\[$(tput bold)\][\u\[$(tput sgr0)\]\[$(tput sgr0)\]\[$(tput bold)\]@\H\[$(tput sgr0)\]]\[$(tput bold)\]\[\033[38;5;196m\]::\[$(tput sgr0)\][\[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;10m\]\w\[$(tput sgr0)\]]$ \[$(tput sgr0)\]"
export LANG='en_US.UTF-8'
alias vi='vim'
export EDITOR='vi'
bind -x '"\C-l": clear'
set colored-stats on
alias ctags='/usr/local/bin/ctags'
alias ctags='/usr/local/bin/ctags'
if [[ $(uname) =~ "Linux" ]]; then
  [ -f /etc/bash_completion ] && . /etc/bash_completion
else
  [ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion
fi

export GO111MODULE=on
export GOPATH=$HOME
export PATH=$PATH:/usr/local/go/bin:$HOME/bin
alias cdtcc="cd /home/gjr/go/src/github.com/lucas-clemente/quic-go/"


stty -ixon

#dual() {
#  xrandr --output eDP-1 --auto --output HDMI-2 --left-of eDP-1 --rotate right
#}

#single() {
#  xrandr -s 0
#}

export LD_LIBRARY_PATH=/usr/local/lib

# history size
HISTSIZE=10000
HISTFILESIZE=10000
HISTCONTROL=ignoreboth:erasedups
HISTTIMEFORMAT="%h %d %H:%M:%S> "
HISTIGNORE="history*"
shopt -s histappend
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"

alias emacsclient="emacsclient -nc"
