-- xmonad.hs configuration file
-- Gaspar Andrade - 8 June 2022
import XMonad
import XMonad.Actions.MouseResize
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.ThreeColumns
import XMonad.Layout.WindowArranger
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.Renamed
import XMonad.Layout.LimitWindows
import XMonad.Layout.SimplestFloat
import XMonad.Layout.SimplestFloat
import XMonad.Util.CustomKeys
import XMonad.Util.EZConfig
import XMonad.Util.Run
import qualified Data.Map as M
import qualified XMonad.Layout.IndependentScreens as LIS
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.StackSet as W

import XMonad.Layout.SimpleDecoration
    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

  -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.


    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))


windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myBorderWidth :: Dimension
myBorderWidth = 2

myFont :: String
myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"



myTerminal        = "xterm"
myModKey          = mod4Mask
myNormalBGColor   = "#000000"
myFocusedBGColor  = "#2A58EE"

-- layout hook
--myLayout = tiled ||| Mirror tiled ||| Full
  --where
    --tiled = Tall nmaster delta ratio
    --nmaster = 1   -- Default number of windows in the master pane
    --ratio = 1/2   -- Default proportion of screen occupied by master pane
    --delta = 3/100 -- Percent of screen to increment by when resizing panes

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#aaa"
                 , inactiveColor       = "#bbb"
                 , activeBorderColor   = "#ccc"
                 , inactiveBorderColor = "#ddd"
                 , activeTextColor     = "#eee"
                 , inactiveTextColor   = "#fff"
                 }


tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 3
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion
-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Ubuntu:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }





myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
               where 
                myDefaultLayout =     withBorder myBorderWidth tall
                                  ||| magnify
                                  ||| monocle
                                

-- myWorkspaces = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]
myWorkspaces = [" dev ", " www ", " sys ", " doc ", " vbox ", " chat ", " mus ", " vid ", " gfx "]


myKeys :: [(String, X ())]
myKeys =
  [ ("M-p", spawn "dmenu_run -b -nb black -sb blue")
  , ("M-f", spawn "chromium"                      )

  -- KB_GROUP Windows navigation
  , ("M-m", windows W.focusMaster)  -- Move focus to the master window
  , ("M-j", windows W.focusDown)    -- Move focus to the next window
  , ("M-k", windows W.focusUp)      -- Move focus to the prev window
  , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window

  -- KB_GROUP Layouts
  , ("M-<Tab>", sendMessage NextLayout)           -- Switch to next layout
  --, ("M-<Space>", sendMessage ToggleStruts) -- Toggles noborder/full
  , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full


  ]

main :: IO()
main = do
  --xmproc <- spawnPipe "xmobar"
  xmproc0 <- spawnPipe ("xmobar -x 0 /home/zat/.xmobarrc")
  --xmproc1 <- spawnPipe ("xmobar -x 1 /home/zat/.xmobarrc2")
  xmproc1 <- spawnPipe ("xmobar -x 1 /home/zat/.xmobarrc")

  xmonad $ docks $ defaultConfig 
    { modMask = myModKey
    , terminal = myTerminal
    , borderWidth = myBorderWidth
    , normalBorderColor = myNormalBGColor
    , focusedBorderColor = myFocusedBGColor
    , manageHook = manageDocks <+> manageHook defaultConfig
    --, layoutHook = myLayoutHook
    , layoutHook = showWName' myShowWNameTheme $ myLayoutHook
    --, layoutHook = avoidStruts $ layoutHook defaultConfig
    , handleEventHook = handleEventHook defaultConfig <+> fullscreenEventHook

    --, logHook = dynamicLogWithPP $ defaultPP 
    , logHook = dynamicLogWithPP $ xmobarPP
      --{ ppOutput = hPutStrLn xmproc, ppOrder = \(ws:_:t:_) -> [ws,t] 
      { ppOutput = \x -> hPutStrLn xmproc0 x
                      >> hPutStrLn xmproc1 x
      , ppCurrent = xmobarColor "#2A58EE" "" . wrap
                            ("<box type=Bottom width=2 mb=2 color=" ++ "#fff" ++ ">") "</box>"
      -- Visible but not current workspace
      , ppVisible = xmobarColor "#2A58EE" ""  
      -- Title of active window
      , ppTitle = xmobarColor "#2A58EE" "" . shorten 60
       -- Hidden workspace
       , ppHidden = xmobarColor "#fff" "" . wrap
                            ("<box type=Top width=2 mt=2 color=" ++ "#fff" ++ ">") "</box>" 
       -- Hidden workspaces (no windows)
       , ppHiddenNoWindows = xmobarColor "#403e3c" "" 

      , ppSep =  "<fc=" ++ "#666666" ++ "> <fn=5>|</fn> </fc>"
      -- Adding # of windows on current workspace to the bar
      , ppExtras  = [windowCount]
      , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
      }
    } `additionalKeysP` myKeys
