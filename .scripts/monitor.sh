#!/bin/bash -e

if [[ $1 == "left" ]]; then
  xrandr --output eDP-1 -s 1920x1080 --auto --output HDMI-2 --left-of eDP-1 --mode 1920x1080 --rotate right --dpi 96
elif [[ $1 == "right" ]]; then
  xrandr --output eDP-1 -s 1920x1080 --auto --output HDMI-2 --right-of eDP-1 --mode 1920x1080 --rotate right --dpi 96
else
  echo -e "usage $0 <second-monitor-side>\ne.g. $0 left"
fi
